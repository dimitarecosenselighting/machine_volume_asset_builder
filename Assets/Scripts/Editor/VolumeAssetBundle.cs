﻿
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class VolumeAssetBundle : ScriptableObject
{

	//public string InputFileNameReg;

	//Shader_Images_0000000.0000_in_8_bit_000000
	//Shader_Images_0000000.5000_in_8_bit_000001
	//Shader_Images_0000001.0000_in_8_bit_000002

	//Shader_Images_0000020.0000_in_8_bit_000040
	//Shader_Images_0000025.0000_in_8_bit_000041

	public static string InputImagesPath ;
	public static string OutpuBundlePathIos  = "Assets/Output/Ios/";
	public static string OutpuBundlePathWin  = "Assets/Output/Win";
	public static string FixtureSkuName      = "fixtueSKU";
	public static string OutputBundleFolder;
	public static string OutputAssetPath;
	public static string OnCompleteRun;

	public static Texture3D CreateVolume3d(Texture2D[] images)
	{
		List<Color> colors = new List<Color>(images[0].width * images[0].height * images.Length);

		Texture3D tx3 = new Texture3D(images[0].width,
										images[0].height,
										images.Length,
										TextureFormat.RGBA32, false);

		tx3.wrapMode = TextureWrapMode.Clamp;
		tx3.filterMode = FilterMode.Bilinear;

		for (int z = 0; z < images.Length; z++)
		{
			colors.AddRange(images[z].GetPixels());
		}
		tx3.SetPixels(colors.ToArray(), 0);

		tx3.Apply(false);

		return tx3;
	}

	static public Texture2D LoadImageToTexture(string filePath)
	{
		Texture2D tex = null;
		byte[] fileData;

		if (File.Exists(filePath))
		{
			fileData = File.ReadAllBytes(filePath);
		
			tex = new Texture2D(2, 2,TextureFormat.RGBA32,false);

			tex.LoadImage(fileData);
			tex.Apply();
		}
		else
		{
			Debug.Log(filePath + " does not exist");
		}
		return tex;
	}

	static void CreateOutputFolders()
	{
		foreach (var folder in new[] { OutpuBundlePathIos, OutpuBundlePathWin, OutputAssetPath })
		{
			if (Directory.Exists(folder))
			{
				DirectoryInfo di = new DirectoryInfo(folder);

				foreach (FileInfo file in di.GetFiles())
				{
					file.Delete();
				}
				foreach (DirectoryInfo file in di.GetDirectories())
				{
					di.Delete(true);
				}
			}
			else
			{
				Directory.CreateDirectory(folder);
			}
		}
	}


	static void CreateVolumeFromImages()
	{
		CreateOutputFolders();

		DirectoryInfo dirVol1 = new DirectoryInfo(InputImagesPath + "/volume1/");
		DirectoryInfo dirVol2 = new DirectoryInfo(InputImagesPath + "/volume2/");

		FileInfo[] infoVol1 = dirVol1.GetFiles("*.png");
		FileInfo[] infoVol2 = dirVol1.GetFiles("*.png");

		Texture2D[] textureVol1 = new  Texture2D[infoVol1.Length];
		Texture2D[] textureVol2 = new  Texture2D[infoVol2.Length];

		int i = 0;
		foreach (var f in infoVol1)
		{
			textureVol1[i] = (LoadImageToTexture(f.FullName));
			i++;
		}

		i = 0;
		foreach (var f in infoVol2)
		{
			textureVol2[i] = (LoadImageToTexture(f.FullName));
			i++;
		}

		var voulme1 = CreateVolume3d(textureVol1);
		var voulme2 = CreateVolume3d(textureVol2);

		AssetDatabase.CreateAsset(voulme1, OutputAssetPath + FixtureSkuName + "_volume_1" + ".asset");	
    	AssetDatabase.CreateAsset(voulme2, OutputAssetPath + FixtureSkuName + "_volume_2" + ".asset");

		AssetImporter importerVol1 = AssetImporter.GetAtPath(OutputAssetPath + FixtureSkuName + "_volume_1" + ".asset");
		AssetImporter importerVol2 = AssetImporter.GetAtPath(OutputAssetPath + FixtureSkuName + "_volume_2" + ".asset");

		importerVol1.assetBundleName = FixtureSkuName + ".bundle";
		importerVol2.assetBundleName = FixtureSkuName + ".bundle";
	}

	private static string GetArg(string name)
	{
		var args = System.Environment.GetCommandLineArgs();
		for (int i = 0; i < args.Length; i++)
		{
			if (args[i] == name && args.Length > i + 1)
			{
				return args[i + 1];
			}
		}
		return null;
	}


	//[MenuItem("Tools/Build Prefab")]
	public static void ExecBuildAssetBundles()
	{

		InputImagesPath      = GetArg("-inputImageFolder")     ?? "D:\\Trov_Image_Data";
		OutputBundleFolder   = GetArg("-outputBundleFolder")   ?? "D:\\VolumeData";
		FixtureSkuName       = GetArg("-sku") ?? "fixtureSku";
		OnCompleteRun        = GetArg("-onCompleteRun");

		OutpuBundlePathIos = OutputBundleFolder + "/" + FixtureSkuName + "/ios/";
   	    OutpuBundlePathWin = OutputBundleFolder + "/" + FixtureSkuName + "/win/";
		OutputAssetPath = "Assets/Output/" + FixtureSkuName + "/";

		CreateVolumeFromImages();

		Debug.Log("Building bundle");
		BuildPipeline.BuildAssetBundles(OutpuBundlePathIos, BuildAssetBundleOptions.None, BuildTarget.iOS);
		BuildPipeline.BuildAssetBundles(OutpuBundlePathWin, BuildAssetBundleOptions.None, BuildTarget.StandaloneWindows);


		System.Diagnostics.Process.Start(OnCompleteRun, FixtureSkuName);
		
	}
}
